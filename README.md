This is a small script for downloading latest students' repos of a given course. 

# Dependency
Install python-gitlab by running:
`pip install python-gitlab==1.4`
We can only use 1.4 version because gitlab in uwaterloo is still using gitlab v3 api.

#  Get private token
Before running the script, you need to get a private token for GitLab. This is to let the script be able to access your account without requiring your username and password. The steps is shown in pictures below:

![steps for getting private token](./img/token_1.png "steps for getting private token")
![private token](./img/token_2.png "private token")

# Put it into the script
After getting that token, you need to paste it in the script. Also, change the path for saving those repos and change the matching condition for repos as necessary.

# Have a coffee and run the script!

