import gitlab
import subprocess

save_path = 'path/to/save'
token = 'XXXXXXXXXXXXXXXXXXXX'

gl = gitlab.Gitlab(url='https://git.uwaterloo.ca', api_version=3, private_token = token)
groups = gl.groups.list()
for group in groups:
    # change the string here accordingly
    if str(group.path) == 'ece000-1191':
        g = gl.groups.get(group.id)
        page_index = 0
        projects = g.projects.list(page = page_index)
        while len(projects) > 0:
            for prj in projects:
                # change the string here accordingly
                if 'ece000-1191-a1-' in prj.name:
                    print prj.name, prj.id
                    prj_url = 'https://git.uwaterloo.ca/api/v3/projects/' + str(prj.id) + '/repository/archive?private_token=' + token
                    print prj_url
                    print save_path + prj.name
                    subprocess.call(['wget', prj_url, '-O', save_path + prj.name])  # blocking
            page_index += 1
            projects = g.projects.list(page = page_index)

